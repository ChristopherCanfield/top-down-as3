package com.divergentthoughtsgames.assets 
{
	/**
	 * ...
	 * @author Christopher D. Canfield
	 */
	public class Graphics 
	{
		[Embed(source = '../../../../res/art/zelda-chickens.png')]
		public const Chickens:Class;
		
		[Embed(source = '../../../../res/art/pacman-ghost.png')]
		public const Ghost:Class;
	}
}