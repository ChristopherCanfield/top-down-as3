package com.divergentthoughtsgames.assets 
{
	/**
	 * ...
	 * @author Christopher D. Canfield
	 */
	public class Audio 
	{
		// ==== Music ====
		
		[Embed(source = '../../../../res/sound/music-butracker.mp3')]
		public const MusicTrack1: Class;
		
		// ==== Sound Effects ====
		
		// Explosion sound effects.
		
		[Embed(source = '../../../../res/sound/explosion1.mp3')]
		public const Explosion1: Class;
		
		[Embed(source = '../../../../res/sound/explosion2.mp3')]
		public const Explosion2: Class;
		
		[Embed(source = '../../../../res/sound/explosion3.mp3')]
		public const Explosion3: Class;
		
		// Hurt sound effects.
		
		[Embed(source = '../../../../res/sound/hurt1.mp3')]
		public const Hurt1: Class;
		
		[Embed(source = '../../../../res/sound/hurt2.mp3')]
		public const Hurt2: Class;
		
		[Embed(source = '../../../../res/sound/hurt3.mp3')]
		public const Hurt3: Class;
		
		[Embed(source = '../../../../res/sound/hurt4.mp3')]
		public const Hurt4: Class;
		
		// Fire weapon sound effects.
		
		[Embed(source = '../../../../res/sound/machineGunAttack-1.mp3')]
		public const MachineGunAttack1: Class;
		
		[Embed(source = '../../../../res/sound/machineGunAttack-2.mp3')]
		public const MachineGunAttack2: Class;
		
		[Embed(source = '../../../../res/sound/machineGunAttack-3.mp3')]
		public const MachineGunAttack3: Class;
		
		[Embed(source = '../../../../res/sound/machineGunAttack-4.mp3')]
		public const MachineGunAttack4: Class;
		
		[Embed(source = '../../../../res/sound/sniperRifleAttack-1.mp3')]
		public const SniperRifleAttack: Class;
	}
}