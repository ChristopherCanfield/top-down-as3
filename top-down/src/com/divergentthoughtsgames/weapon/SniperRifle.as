package com.divergentthoughtsgames.weapon 
{
	import com.divergentthoughtsgames.assets.Assets;
	
	import org.flixel.FlxSprite;
	import org.flixel.FlxState;
	import org.flixel.FlxTilemap;
	import org.flixel.FlxRect;
	import org.flixel.FlxG;
	import org.flixel.plugin.photonstorm.FlxMath;
	import org.flixel.plugin.photonstorm.FlxWeapon;
	
	/**
	 * A sniper rifle weapon (slow firing, powerful bullets, long range).
	 * @author Christopher D. Canfield
	 */
	public class SniperRifle extends FlxWeapon
	{	
		public function SniperRifle(owner: FlxSprite, gameState: FlxState, level: FlxTilemap) 
		{
			super("Sniper Rifle", owner, "x", "y");
			
			makePixelBullet(5, 3, 3, 0xff000000);
			setBulletLifeSpan(3000);
			setBulletSpeed(250);
			setBulletBounds(new FlxRect(0, 0, level.width, level.height));
			setBulletElasticity(0.5);
			setFireRate(1000);
			gameState.add(group);
			
			setFireCallback(onFire)
		}
		
		private function onFire(): void
		{
			FlxG.play(Assets.audio.SniperRifleAttack, 0.5);
		}
	}

}