package com.divergentthoughtsgames.weapon 
{
	import com.divergentthoughtsgames.assets.Assets;
	
	import org.flixel.FlxSprite;
	import org.flixel.FlxState;
	import org.flixel.FlxTilemap;
	import org.flixel.FlxRect;
	import org.flixel.FlxG;
	import org.flixel.plugin.photonstorm.FlxMath;
	import org.flixel.plugin.photonstorm.FlxWeapon;
	
	/**
	 * A machine gun weapon (quick firing, weak bullets).
	 * @author Christopher D. Canfield
	 */
	public class MachineGun extends FlxWeapon
	{
		private var attackSounds: Vector.<Class>;
		
		public function MachineGun(owner: FlxSprite, gameState: FlxState, level: FlxTilemap) 
		{
			super("Machine Gun", owner, "x", "y");
			
			makePixelBullet(50, 3, 3, 0xff000000);
			setBulletLifeSpan(2000);
			setBulletSpeed(200);
			setBulletBounds(new FlxRect(0, 0, level.width, level.height));
			setBulletElasticity(0.5);
			setFireRate(150);
			gameState.add(group);
			
			attackSounds = new Vector.<Class>();
			attackSounds.push(Assets.audio.MachineGunAttack1, 
						Assets.audio.MachineGunAttack2,
						Assets.audio.MachineGunAttack3,
						Assets.audio.MachineGunAttack4);
			
			setFireCallback(onFire)
		}
		
		private function onFire(): void
		{
			var soundIndex:int = FlxMath.rand(0, attackSounds.length - 1);
			FlxG.play(attackSounds[soundIndex], 0.25);
		}
	}

}