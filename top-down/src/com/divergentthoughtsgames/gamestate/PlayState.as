package com.divergentthoughtsgames.gamestate {
	import com.divergentthoughtsgames.Ghost;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.utils.getTimer;
	import org.flixel.FlxGroup;
	import org.flixel.FlxParticle;
	import org.flixel.FlxSound;
	import org.flixel.plugin.photonstorm.FlxDelay;
	
	import org.flixel.FlxRect;
	import org.flixel.FlxState;
	import org.flixel.FlxSprite;
	import org.flixel.FlxText;
	import org.flixel.FlxTilemap;
	import org.flixel.FlxG;
	import org.flixel.FlxObject;
	import org.flixel.FlxCamera;
	import org.flixel.FlxEmitter;
	import org.flixel.plugin.photonstorm.FlxMath;
	import org.flixel.plugin.photonstorm.FlxCollision;
	import org.flixel.plugin.photonstorm.BaseTypes.Bullet;
		
	import net.pixelpracht.tmx.*;
	
	import com.bit101.utils.MinimalConfigurator;
	import com.bit101.components.Component;
	import com.bit101.components.InputText;
	
	import com.divergentthoughtsgames.assets.Assets;
	import com.divergentthoughtsgames.VariableEditorWindow;
	import com.divergentthoughtsgames.Player;
	
	/**
	 * ...
	 * @author Christopher D. Canfield
	 */
	public class PlayState extends FlxState
	{
		// The level's map.
		private var level:FlxTilemap;
		
		// Entities.
		private var player:Player;
		private var ghosts:FlxGroup;
		
		// Sounds.
		private var bulletHitSounds:Vector.<Class>;
		private var hitSounds:Vector.<Class>;
		private var hitSoundTimer:FlxDelay;
		
		private var timeRemainingText:FlxText;
		private var livesText:FlxText;
		
		private var instructionsText:FlxText;
		
		// The number of seconds per level.
		private const LEVEL_TIME:Number = 999;
		private var timeRemaining:Number = LEVEL_TIME;
		
		private var gameOver:Boolean = false;
		
		private var debugConfig:MinimalConfigurator;
 
        override public function create():void
        {
            FlxG.bgColor = 0xffaaaaaa;
			
			loadMap();
			
			//Create player (a red box)
			var startX:int = FlxG.width / 2 - 5;
			var startY:int = 2080;
			
			// Add a play instructions.
			instructionsText = new FlxText(startX - 150, startY - 300, 300);
			instructionsText.size = 15;
			instructionsText.text = "WASD or Arrow Keys to move Mouse click or spacebar to shoot";
			instructionsText.shadow = 0xff000000;
			add(instructionsText);
			
			player = new Player(this, level, startX, startY);
			FlxG.camera.follow(player, FlxCamera.STYLE_TOPDOWN);
			add(player);
			
			ghosts = new FlxGroup(3);
			ghosts.add(new Ghost(this, level, startX - 100, startY - 100, player));
			ghosts.add(new Ghost(this, level, startX + 100, startY, player));
			ghosts.add(new Ghost(this, level, startX, startY + 100, player));
			
			add(ghosts);
			
			// Add a label for the score.
			timeRemainingText = new FlxText(2, 2, 80);
			timeRemainingText.scrollFactor.x = timeRemainingText.scrollFactor.y = 0;
			timeRemainingText.shadow = 0xff000000;
			timeRemainingText.text = "Time: " + Math.round(timeRemaining);
			add(timeRemainingText);
			
			// Add a label for the lives.
			livesText = new FlxText(FlxG.width / FlxG.camera.getScale().x - 45,  4, 45);
			livesText.scale = FlxG.camera.getScale();
			livesText.scrollFactor.x = livesText.scrollFactor.y = 0;
			livesText.shadow = 0xff000000;
			livesText.antialiasing = true;
			add(livesText);
			
			setSounds();
			FlxG.playMusic(Assets.audio.MusicTrack1, 0.8);
			
			// Add the variable editor window. Remove this for release builds.
			var variableEditorWindow:VariableEditorWindow = new VariableEditorWindow();
			debugConfig = variableEditorWindow.create(player);
        }
		
		private function setSounds(): void
		{
			bulletHitSounds = new Vector.<Class>();
			bulletHitSounds.push(Assets.audio.Explosion1);
			bulletHitSounds.push(Assets.audio.Explosion2);
			bulletHitSounds.push(Assets.audio.Explosion3);
			
			hitSounds = new Vector.<Class>();
			hitSounds.push(Assets.audio.Hurt1);
			hitSounds.push(Assets.audio.Hurt2);
			hitSounds.push(Assets.audio.Hurt3);
			hitSounds.push(Assets.audio.Hurt4);
			
			hitSoundTimer = new FlxDelay(50);
			hitSoundTimer.start();
			
			FlxG.mute = false;
			FlxG.volume = 1;
		}
		
		public function removeInstructions(): void
		{
			if (instructionsText != null)
			{
				remove(instructionsText, true);
				instructionsText = null;
			}
		}
		
		private function loadMap() : void 
		{
			var xml:XML = new XML(new Assets.level.Level1Xml());
			var tmx:TmxMap = new TmxMap(xml);
			
			var tileset:TmxTileSet = tmx.getTileSet('warcraft-tileset-2');
			var mapCsv:String = tmx.getLayer('map').toCsv(tileset);
			level = new FlxTilemap();
			level.loadMap(mapCsv, Assets.level.Level1Image, 32, 32, FlxTilemap.OFF, 0, 0, 196);
			add(level);
			
			var scaleX:Number = FlxG.camera.getScale().x;
			var scaleY:Number = FlxG.camera.getScale().y;
			
			FlxG.worldBounds = level.getBounds();
			FlxG.worldBounds.width = level.width + 32;
			FlxG.camera.setBounds(0, 0, level.width * scaleX - 410, level.height * scaleY - 460, true);
		}
		
		override public function update(): void
		{
			if (gameOver)
			{
				return;
			}
			
			super.update();
			
			// Process collisions.
			FlxG.collide(level, player);
			FlxG.collide(level, player.getBulletGroup(), bulletCollider);
			FlxG.collide(level, ghosts);
			FlxG.collide(ghosts, player.getBulletGroup(), bulletCollider);
			FlxG.collide(player, ghosts, ghostCollider);
			
			timeRemaining -= FlxG.elapsed;
			timeRemainingText.text = "Time: " + Math.round(timeRemaining);
			gameOver = ((timeRemaining <= 0) ? true : 
					(player.getLives() == 0) ? true : false);
			if (gameOver)
			{
				onGameOver();
			}
			
			livesText.text = "Lives: " + player.getLives();
		}
		
		private function onGameOver() : void
		{
			FlxG.mouse.show();
            FlxG.switchState(new LoseMenuState());
		}
		
		private function bulletCollider(object1: FlxObject, bullet: Bullet): void
		{
			if (bullet.isTouching(0x1111))
			{
				//bullet.expiresTime = getTimer() + 125;
				bullet.kill();
				
				var emitter:FlxEmitter = new FlxEmitter(bullet.x, bullet.y);
				var particles:int = 5;
	 
				for(var i:int = 0; i < particles; i++)
				{
					var particle:FlxParticle = new FlxParticle();
					particle.makeGraphic(2, 2, 0xffffffff);
					particle.exists = false;
					emitter.add(particle);
				}
				
				add(emitter);
				emitter.start(true, 0.5);
				
				var soundIndex:int = FlxMath.rand(0, bulletHitSounds.length - 1);
				FlxG.play(bulletHitSounds[soundIndex]);
			}
		}
		
		private function ghostCollider(player: Player, ghost: Ghost): void
		{
			if (player.isTouching(0x1111) && ghost.isTouching(0x1111) && hitSoundTimer.hasExpired)
			{
				var soundIndex:int = FlxMath.rand(0, hitSounds.length - 1);
				FlxG.play(hitSounds[soundIndex]);
				
				hitSoundTimer.reset(400);
				hitSoundTimer.start();
			}
		}
	}
}