# Top-Down Action Template #

An ActionScript 3 template for prototyping top-down action style games. Uses the Flixel game framework.

[Try it out](http://christopherdcanfield.com/projects/ludumdare/30/templates/top-down/)

![top-down-action.jpg](https://bitbucket.org/repo/pABeoM/images/2718939040-top-down-action.jpg)

Christopher D. Canfield  
September 2014